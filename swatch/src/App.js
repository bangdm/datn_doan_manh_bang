import React, { Component, lazy, Suspense } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

import Detailproduct from './components/details/detailproduct';
import Footer from './components/layers/Footer';
import Notfound from './components/layers/Notfound.component';
import Search from './components/products/search';
import Filterproducts from './components/products/filterproducts';
import Contact from './components/layers/Contact';

import Applepage from './components/pageProducts/apple.page';
import Samsungpage from './components/pageProducts/samsung.page';
import Xiaomipage from './components/pageProducts/xiaomi.page';
import Huaweipage from './components/pageProducts/huawei.page';
import Count from './components/count';
import Bill from './components/details/bill';
import Public from './components/layers/public';

const MyComp = lazy(() => import('./components/layers/Hearder'));

export default class App extends Component {
    render() {
        const Spinner = <FontAwesomeIcon icon={faSpinner} />
        return (
            <Suspense fallback={<div className="text-center" style={{ fontSize: 50, marginTop: "25%" }}>
                {Spinner} &nbsp;loading...
            </div>}>
                <Router>
                    <div className="over-flow bg-light" style={{ backgroundColor: "gainsboro" }}>
                        <MyComp></MyComp><hr /><br /><br />
                        <Switch>
                            <Route exact path="/" component={Filterproducts} />
                            <Route path="/apple" component={Applepage} />
                            <Route path="/samsung" component={Samsungpage} />
                            <Route path="/xiaomi" component={Xiaomipage} />
                            <Route path="/huawei" component={Huaweipage} />
                            <Route path="/detail/:id" component={Detailproduct} />
                            <Route path="/search" component={Search} />
                            <Route path="/contact" component={Contact} />
                            <Route path="/query/:id" component={Count} />
                            <Route path="/bill" component={Bill} />
                            <Route path="/group-public" component={Public} />
                            <Route component={Notfound} />
                        </Switch>
                        <Footer />
                    </div>
                </Router>
            </Suspense>
        );
    }
}
