import React, { Component } from 'react';
import Huawei from '../products/huawei';

export default class Applepage extends Component {
    render() {
        return (
            <div className="container">
                <hr />
                <h3 className="text-center"><strong>NHỮNG SẢN PHẨM GIỚI HẠN HUAWEI</strong></h3>
                <hr />
                <Huawei />
            </div>
        )
    }
}
