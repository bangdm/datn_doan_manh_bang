import React, { Component } from 'react';
import Xiaomi from '../products/xiaomi';

export default class Applepage extends Component {
    render() {
        return (
            <div className="container">
                <hr />
                <h3 className="text-center"><strong>NHỮNG SẢN PHẨM GIỚI HẠN XIAOMI</strong></h3>
                <hr />
                <Xiaomi />
            </div>
        )
    }
}
