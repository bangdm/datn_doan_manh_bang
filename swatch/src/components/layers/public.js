import React, { Component } from 'react';
import Iframe from 'react-iframe';
import swal from '@sweetalert/with-react';

export default class Public extends Component {

    onClick = () => {
        swal("Successful purchases", "Loading ...", "warning");
    }
    render() {
        return (
            <div className="container"><hr />
                <h2 className="text-center">Group public ( Chat )</h2><br />
                <div className="row">
                    <div className="col-5 border">
                        <h5>Hay nhap nut like de theo doi fanpage.</h5><br />
                        <Iframe className="iframe-fb" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=450&layout=standard&action=like&size=small&share=true&height=35&appId=728127094590611"
                            style={{ border: 'none', overflow: 'hidden', height: 45, width: 350 }} scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></Iframe>
                        <Iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=728127094590611"
                            style={{ border: 'none', overflow: 'hidden', width: 340, height: 500 }} scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></Iframe>
                    </div>
                    <div className="col-7 text-center border">
                        <h4>Chat</h4><br />
                        <div className="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width=""></div>
                    </div>
                    <button onClick={this.onClick}>Click me!</button>
                </div><br />
            </div>
        )
    }
}
