import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Contact extends Component {
    render() {
        return (
            <div className="container"><hr />
                <h4 className="text-center">THÔNG TIN SHOP VÀ ĐỊA CHỈ</h4><br />
                <div className="row">
                    <div className="col-5">
                        <div className="list-group" style={{ height: 600, overflow: "auto" }}>
                            <Link to="#" className="list-group-item list-group-item-action flex-column align-items-start active">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1"><b>THÔNG TIN SWATCH</b></h5>
                                </div>
                            </Link>
                            <Link to="#" className="list-group-item list-group-item-action flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">Liên hệ : <b>0978.439.197</b></h5>
                                    <small>Đã cập nhật</small>
                                </div>
                                <p className="mb-1"></p>
                                <small>Lưu ý: Chỉ nghe máy trong giờ hành chính.</small>
                            </Link>
                            <Link to="#" className="list-group-item list-group-item-action flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">Đặt hàng: <b>1900.6666</b></h5>
                                    <small className="text-muted">Đã cập nhật</small>
                                </div>
                                <p className="mb-1"></p>
                                <small>Lưu ý: Chỉ nghe máy trong giờ hành chính.</small>
                            </Link>
                            <Link to="#" className="list-group-item list-group-item-action flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">Bảo hành: <b>1900.7777</b></h5>
                                    <small className="text-muted">Đã cập nhật</small>
                                </div>
                                <p className="mb-1"></p>
                                <small>Lưu ý: Chỉ nghe máy trong giờ hành chính.</small>
                            </Link>
                            <Link to="#" className="list-group-item list-group-item-action flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">Hỗ trợ: <b>1900.8888</b></h5>
                                    <small className="text-muted">Đã cập nhật</small>
                                </div>
                                <p className="mb-1"></p>
                                <small>Lưu ý: Chỉ nghe máy trong giờ hành chính.</small>
                            </Link>
                            <Link to="#" className="list-group-item list-group-item-action flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">Khiếu nại: <b>1900.9999</b></h5>
                                    <small className="text-muted">Đã cập nhật</small>
                                </div>
                                <p className="mb-1"></p>
                                <small>Lưu ý: Chỉ nghe máy trong giờ hành chính.</small>
                            </Link>
                            <Link to="#" className="list-group-item list-group-item-action flex-column align-items-start">
                                <div className="d-flex w-100 justify-content-between">
                                    <h5 className="mb-1">Mail: <b>LuxurySwatch.@email.com.vn</b></h5>
                                    <small className="text-muted">Đã cập nhật</small>
                                </div>
                                <p className="mb-1"></p>
                                <small>Lưu ý: đây là mail chính của website. tránh bị lợi dụng dưới bất kỳ hình thức.</small>
                            </Link>
                        </div>
                    </div>
                    <div className="col-7">
                        <h4>Địa chỉ Shop hiện tại</h4>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9425178403444!2d105.7634885153851!3d21.034985885994974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454b93cc3e515%3A0x2205680ffdf9293c!2zMjQgUGjhu5EgTmd1eeG7hW4gQ8ahIFRo4bqhY2gsIE3hu7kgxJDDrG5oLCBU4burIExpw6ptLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1588068540723!5m2!1svi!2s"
                            style={{ width: 635, height: 600, frameBorder: 0, allowFullScreen: 0, tabIndex: 0 }} aria-hidden="false" title="ifram"></iframe>
                    </div>
                </div><hr />
            </div>
        )
    }
}
