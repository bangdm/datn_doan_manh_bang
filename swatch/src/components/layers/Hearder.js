import React, { Component } from 'react';
import '../../css/Hearder.css';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faSearch, faClock, faHeadphones, faUsers, faIdCard, faPhone, faCartPlus } from '@fortawesome/free-solid-svg-icons';

import Support from '../details/support';

export default class Hearder extends Component {
    render() {
        const Home = <FontAwesomeIcon icon={faHome} />
        const Search = <FontAwesomeIcon icon={faSearch} />
        const Clock = <FontAwesomeIcon icon={faClock} />
        const Users = <FontAwesomeIcon icon={faUsers} />
        const Headphone = <FontAwesomeIcon icon={faHeadphones} />
        const Card = <FontAwesomeIcon icon={faIdCard} />
        const Phone = <FontAwesomeIcon icon={faPhone} />
        const Cart = <FontAwesomeIcon icon={faCartPlus} />
        return (
            <div>
                <button type="button" className="btn btn-primary res" style={{ marginTop: "40%", position: "fixed", border: 0, zIndex: 100 }} data-toggle="modal" data-target="#sideModalTR">
                    Tư vấn khách hàng &nbsp; {Phone}
                </button>
                <div className="modal fade right" id="sideModalTR" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true">
                    <div className="modal-dialog modal-side modal-bottom-right" style={{ float: "left", marginTop: "30%" }} role="document">
                        <div className="modal-content" style={{ width: "80%" }}>
                            <div className="modal-body bg-dark">
                                <button type="button" className="close text-white" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <Support />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bg-dark" style={{ position: "fixed", width: "100%", top: 0, zIndex: 100 }}>
                    <div className="container bg-danger">
                        <nav className="navbar navbar-expand-lg navbar-light bg-dark">
                            <Link to="/">
                                <img src="./Images/LOGO_swatch_luxury.png" style={{ width: "100%" }} className="img-responsive" alt="LogoSwatch" />
                            </Link>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>

                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav mr-auto" style={{ margin: "auto" }}>
                                    <li className="nav-item active">
                                        <Link className="nav-link nav-links text-center" to="/">{Home}<br />Trang chủ <span className="sr-only">(current)</span></Link>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <Link className="nav-link nav-links dropdown-toggle text-center" to="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {Clock}<br />Hãng đồng hồ
                                    </Link>
                                        <div className="dropdown-menu drops" aria-labelledby="navbarDropdown">
                                            <Link className="dropdown-item cs" to="/apple">Apple</Link>
                                            <Link className="dropdown-item cs" to="/samsung">Samsung</Link>
                                            <Link className="dropdown-item cs" to="/xiaomi">Xiaomi</Link>
                                            <Link className="dropdown-item cs" to="huawei">Huawei</Link>
                                            <div className="dropdown-divider"></div>
                                            <Link className="dropdown-item cs" to="/search">Tìm kiếm</Link>
                                        </div>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link nav-links text-center" to="#">{Headphone}<br />Phụ kiện</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link nav-links text-center" to="/group-public">{Users}<br />Cộng đồng</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link nav-links text-center" to="/contact">{Card}<br />Liên hệ</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link type="button" className="nav-link nav-links text-center" to="/search" >{Search} <br />Tìm kiếm</Link>
                                    </li>
                                </ul>
                                <ul className="navbar-nav mr-left">
                                    <li className="nav-item">
                                        <Link type="button" className="nav-link nav-links text-center" to="/bill" >{Cart} <br /> Đơn hàng</Link>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}
