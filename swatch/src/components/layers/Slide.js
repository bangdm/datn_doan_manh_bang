import React from 'react';
import '../../css/news.css';

export default class Menu extends React.Component {
    render() {
        return (
            <div>
                <div className="container navbar-expand-lg navbar-light news"><br />
                    <div className="row collapse navbar-collapse" id="navbarSupportedContents">
                        <div className="col-8">
                            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                                <ol className="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <img className="d-block w-100" src="./Images/dong-ho-thong-minh-1200-450-1200x450-(1).png" alt="First slide" />
                                    </div>
                                    <div className="carousel-item">
                                        <img className="d-block w-100" src="./Images/samsung-wacth-1200-450-1200x450-(1).png" alt="Second slide" />
                                    </div>
                                    <div className="carousel-item">
                                        <img className="d-block w-100" src="./Images/Apple-watch-1200-450-1200x450-(1).png" alt="Third slide" />
                                    </div>
                                </div>
                                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Previous</span>
                                </a>
                                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Next</span>
                                </a>
                            </div><br />
                            <div>
                                <img className="d-block w-100" src="./Images/1200-75-1200x75.gif" alt="Second slide" /><br />
                                <img className="d-block w-100" src="./Images/637205757372419373_H5.jpeg" alt="Second slide" />
                            </div>
                        </div>

                        <div className="col-4 navbar-expand-lg navbar-light news">
                            <nav className="s">
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContents" aria-controls="navbarSupportedContents" aria-expanded="false" aria-label="Toggle navigation">
                                </button>
                                <div className="collapse navbar-collapse" id="navbarSupportedContents">
                                    <img src="https://donghohungthinh.com/uploads/goithieu/cndlcitizen.jpg"
                                        alt="cam ket"
                                        style={{ width: '100%' }}
                                        className="media-css"
                                    />
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
