import React from 'react';
import '../../css/Footer.css';
import { Link } from 'react-router-dom';
import Iframe from 'react-iframe';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretSquareUp, faShippingFast, faTools, faSyncAlt } from '@fortawesome/free-solid-svg-icons';

export default class Footer extends React.Component {
	render() {
		const Caretsquareup = <FontAwesomeIcon icon={faCaretSquareUp} />
		const Shippingfast = <FontAwesomeIcon icon={faShippingFast} />
		const Tools = <FontAwesomeIcon icon={faTools} />
		const Syncalt = <FontAwesomeIcon icon={faSyncAlt} />
		return (
			<div>
				<div className="bg-secondary">
					<div className="container">
						<nav className="navbar navbar-expand-lg navbar-light bg-secondary">
							<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContents" aria-controls="navbarSupportedContents" aria-expanded="false" aria-label="Toggle navigation">
								<span className="navbar-toggler-icon"></span>
							</button>
							<div className="collapse navbar-collapse" id="navbarSupportedContents">
								<ul className="navbar-nav mr-auto" style={{ margin: "auto" }}>
									<li className="nav-item centers">
										<Link to="#" className="text-white dichvu"><br />{Syncalt}<br />1 ĐỔI 1 TRONG VÒNG 3O NGÀY <br /> NẾU LỖI CỦA NHÀ SẢN XUẤT</Link>
									</li>
								</ul>
								<ul className="navbar-nav mr-auto" style={{ margin: "auto" }}>
									<li className="nav-item centers">
										<Link to="#" className="text-white dichvu"><br />{Shippingfast}<br /> MIỄN PHÍ VẪN CHUYỂN</Link>
									</li>
								</ul>
								<ul className="navbar-nav mr-auto">
									<li className="nav-item centers">
										<Link to="#" className="text-white dichvu"><br />{Tools}<br />TẶNG GÓI BẢO HÀNH NGƯỜI<br />DÙNG TRONG VÒNG 12 THÁNG</Link>
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>

				<div className="bg-dark">
					<div className="container bg-dark">
						<div className="row">
							<div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<br />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">CHĂM SÓC KHÁCH HÀNG</h6>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Hướng dẫn mua hàng</Link>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Trả góp theo lãi xuất</Link>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Chính sách đổi trả</Link>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Chính sách bảo hành</Link>
								</div>
								<hr />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">GIẢI THƯỞNG CHỨNG NHẬN</h6>
									<br />
									<img src="./Images/cup.png" className="img-responsive img-footer" alt="Images" />
									<p className="text-white">Chính sách hậu mãi , được người dùng Việt Nam tin dùng</p>
								</div>
							</div>
							<div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
								<br />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">VỀ LUXURYWATCH</h6>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Giới thiệu</Link>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Tuyển dụng</Link>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Trung tâm bảo hành</Link>
									<Link to="#" className="list-group bg-dark" style={{ textDecoration: "none" }}>Các hệ thống bán lẻ</Link>
								</div><hr />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">Chứng nhận SGD TMĐT</h6>
									<br />
									<img src="./Images/dangky-bct.png" className="img-responsive" style={{ width: 115 }} alt="Đã đăng ký bộ công thương" />
								</div>
							</div>
							<div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
								<br />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">LIÊN HỆ</h6>
									<Link to="#" className="bg-dark" style={{ textDecoration: "none" }}>Gọi đặt hàng <b>1800.6666</b> (8:30 - 22:00)</Link>
									<Link to="#" className="bg-dark" style={{ textDecoration: "none" }}>Gọi bảo hành <b>1800.7777</b> (8:00 - 19:00)</Link>
									<Link to="#" className="bg-dark" style={{ textDecoration: "none" }}>Gọi kỹ thuật <b>1800.8888</b> (8:30 - 22:00)</Link>
									<Link to="#" className="bg-dark" style={{ textDecoration: "none" }}>Gọi khiếu nại <b>1800.9999</b> (8:30 - 22:00)</Link>
								</div>
								<hr />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">TIỆN ÍCH HƠN KHI SỬ DỤNG APP</h6>
									<br />
									<div className="row container">
										<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<img src="./Images/appplay.png" className="img-responsive" alt="Ứng dụng tiện ích google play" />
										</div>
										<div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<img src="./Images/appstore.png" className="img-responsive" alt="Ứng dụng tiện ích app store" />
										</div>
									</div><br />
									<div>
										<Iframe className="iframe-fb" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&width=450&layout=standard&action=like&size=small&share=true&height=35&appId=728127094590611"
											style={{ border: 'none', overflow: 'hidden', height: 45, width: 350 }} scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></Iframe>
									</div>
								</div>
							</div>
							<div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
								<br />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">KẾT NÓI VỚI CHÚNG TÔI</h6>
									<br />
									<Iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffacebook&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=728127094590611"
										style={{ border: 'none', overflow: 'hidden', width: 340, height: 500 }} scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></Iframe>
								</div>
								<hr />
								<div className="list-group">
									<h6 className="list-group disabled bg-dark text-white">CÁC LOẠI THANH TOÁN</h6>
									<br />
									<img src="./Images/thanhtoan-img.png" className="img-responsive" alt="Hình thức thanh toán" />
								</div>
							</div>
						</div>
						<div className="row">
							<p className="text-white text-center"><small>© 2007 - 2019 Công Ty Cổ Phần Bán Lẻ SMARTWATCH / Địa chỉ: 261 Khánh Hội, P5, Q4, TP. Hồ Chí Minh / GPĐKKD số 0311609355 do Sở KHĐT TP.HCM cấp ngày 08/03/2012. GP số 47/GP-TTĐT do sở TTTT TP HCM cấp ngày 02/07/2018. Điện thoại: (098)8888888. Email: smartwatch@gmail.com.vn. Chịu trách nhiệm nội dung: SMARTWATCH.</small></p>
						</div>
					</div>
					<div className="back-to-top">{Caretsquareup}</div>
				</div>
			</div>
		)
	}
}
