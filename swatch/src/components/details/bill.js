import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Bill extends Component {
    render() {
        const localS = JSON.parse(localStorage.getItem('reciept-1800.9999-order'));
        if (localS !== 'undefined' && localS !== null) {
            return (
                <div className="container"><hr />
                    <h6><u>Trang chủ > Đơn hàng</u></h6>
                    <h2 className="text-center">HÓA ĐƠN THANH TOÁN</h2><br />
                    <span><b>Cảm ơn quý khách đã mua hàng tại Swatch.</b> Chúng tôi hân hạnh được phục vụ quý khách trong những lần tới.</span>
                    <p>Mọi thắc mắc liên hệ qua số <b className="bg-warning text-danger">1800.9999</b></p>
                    <h4>THÔNG TIN ĐƠN HÀNG</h4><br />
                    <div className="row border">
                        <div className="col-4 border">
                            <h4 className="text-center">Thông tin khách hàng</h4><br />
                            <p>Tên khách hàng: <b>{localS.nickname}</b></p>
                            <p>Số điện thoại: <b>{localS.phone}</b></p>
                            <p>Email: <b>{localS.email}</b></p>
                            <p>Địa chỉ: <b>{localS.address}</b></p>
                            <p>Ghi chú: <b>{localS.note}</b></p>
                            <p>Thời gian đặt hàng: <b>{localS.date}</b>{}</p>
                        </div>
                        <div className="col-8">
                            <h4 className="text-center">Thông tin sản phẩm</h4><br />
                            <div className="row">
                                <div className="col-4">
                                    <img src={localS.image} alt={localS.name}
                                        height={localS.image ? 190 : 0}
                                        width={localS.image ? 190 : 0}
                                    />
                                </div>
                                <div className="col-8">
                                    <p>Tên sản phẩm: <b>{localS.name}</b></p>
                                    <p>Giá gốc: <b>{(localS.price).toLocaleString()}</b> vnđ.</p>
                                    <p>Giá Giảm: <b>{(localS.sale).toLocaleString()}</b> vnđ.</p>
                                    <p>Số lượng: <b>{localS.amount}</b></p>
                                    <p>Màu sắc: <button className="btn btn-bg" style={{ backgroundColor: localS.color }} /> <b>{localS.color}</b></p>
                                </div>
                            </div>
                        </div>
                    </div> &nbsp;
                    <div className="row">
                        <div className="col-8">
                            <Link className="btn btn-sm btn-danger" to="/">Quay về trang chủ</Link>
                            <p className="text-center"><b className="text-danger">*Lưu ý: </b>Đây là đơn hàng gần nhất bạn đã đặt.</p>
                        </div>
                        <div className="col-4 border text-center">
                            <h6>Tổng giá:  <b className="text-danger">{(localS.totalPrice).toLocaleString()}</b> vnđ.</h6>
                            <h4>Thanh toán:  <b className="text-danger">{(localS.totalPrice).toLocaleString()}</b> vnđ.</h4>
                        </div>
                    </div><hr />
                    <h4>Cộng đồng chat</h4>
                    <div className="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width=""></div>
                </div>
            )
        } else {
            return (
                <div className="container" style={{ backgroundColor: 'white' }}><hr />
                    <h6><u>Trang chủ > Đơn hàng</u></h6>
                    <h2 className="text-center">ĐƠN HÀNG</h2><br />
                    <div className="row">
                        <div className="col-12 text-center">
                            <img src='https://theme.hstatic.net/1000296552/1000391398/14/icon_gio_hang_mobile.png?v=732' alt='gio hang trong'
                                style={{ height: 150, width: 150 }}
                            /> &nbsp;
                            <h3 className="text-center">Không có đơn hàng nào !</h3>
                            <p>Mọi thắc mắc liên hệ qua số <b>1800.9999</b></p>
                        </div>
                        <div className="row container">
                            <div className="col-12">
                                <Link className="btn btn-sm btn-danger" type="button" to='/'>Quay về trang chủ</Link>
                            </div>
                        </div>
                    </div><br />
                </div>
            )
        }

    }
}
