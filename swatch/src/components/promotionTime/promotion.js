import React, { Component } from 'react';

export default class Promotion extends Component {
    render() {
        const countDownDate = new Date("may 30, 2020 23:59:59").getTime();
        const x = setInterval(function () {
            // Lấy thời gian hiện tại
            const now = new Date().getTime();
            // Lấy số thời gian chênh lệch
            const distance = countDownDate - now;
            // Tính toán số ngày, giờ, phút, giây từ thời gian chênh lệch
            const days = Math.floor(distance / (1000 * 60 * 60 * 24));
            const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((distance % (1000 * 60)) / 1000);
            // HIển thị chuỗi thời gian trong thẻ p
            document.getElementById("demo").innerHTML = days + " Days " + hours + " Hours "
                + minutes + " Minutes " + seconds + " Seconds ";
            // Nếu thời gian kết thúc, hiển thị chuỗi thông báo
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "Chương trình khuyến mãi kết thúc";
            }
        }, 1000);
        return (
            <div>
                <p className="text-center" id="demo">Hot Sale</p>
            </div>
        )
    }
}
