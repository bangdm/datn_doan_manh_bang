const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Persons = new Schema({
    manufacturer: {
        type: String
    },
    check: {
        type: String
    },
    status: {
        type: String
    },
    date: {
        type: Date
    },
    amounts: {
        type: Number
    },
    name: {
        type: String
    },
    price: {
        type: Number
    },
    sale: {
        type: Number
    },
    image: {
        type: String
    },
    image_src: {
        type: String
    },
    detail: {
        type: String
    },
    gift: {
        type: String
    },
    gifts: {
        type: String
    },
    promotion: {
        type: String
    },
    promotions: {
        type: String
    },
    pk: {
        type: String
    },
    guarantee: {
        type: String
    },
    guarantees: {
        type: String
    },

    color: {
        type: String
    },
    screen: {
        type: String
    },
    ram: {
        type: String
    },
    rom: {
        type: String
    },
    cpu: {
        type: String
    },
    pin: {
        type: String
    },
    waterproof: {
        type: String
    },
    material: {
        type: String
    },
    description: {
        type: String
    }
}, {
    collection: 'persons'
});

module.exports = mongoose.model('Persons', Persons);
