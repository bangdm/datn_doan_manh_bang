const express = require('express');
const personRoutes = express.Router();

// Require Business model in our routes module
let Person = require('./persons.model');

// Defined store route
personRoutes.route('/add').post(function (req, res) {
    let person = new Person(req.body);
    person.save()
        .then(person => {
            res.status(200).json({ 'person': 'person in added successfully' });
        })
        .catch(err => {
            res.status(400).send("unable to save to database");
        });
});

// Defined pay store route
personRoutes.route('/pay').post(function (req, res) {
    let person = new Person(req.body);
    person.save()
        .then(person => {
            res.status(200).json({ 'person': 'person in added successfully' });
        })
        .catch(err => {
            res.status(400).send("unable to save to database");
        });
});

// Defined get data(index or listing) route
personRoutes.route('/').get(function (req, res) {
    Person.find((err, persons) => {
        if (err) {
            console.log(err);
        }
        else {
            res.json(persons);
        }
    });
});

// Defined detail route
personRoutes.route('/detail/:id').get(function (req, res) {
    let id = req.params.id;
    Person.findById(id, function (err, business) {
        res.json(business);
    });
});

// Defined pay route
personRoutes.route('/pay/:id').get(function (req, res) {
    let id = req.params.id;
    Person.findById(id, function (err, business) {
        res.json(business);
    });
});

// Defined edit route
personRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    Person.findById(id, function (err, business) {
        res.json(business);
    });
});

//  Defined update route
personRoutes.route('/update/:id').post(function (req, res) {
    Person.findById(req.params.id, function (err, person) {
        if (!person)
            res.status(404).send("data is not found");
        else {
            console.log(person);

            person.check = req.body.check;
            person.status = req.body.status;
            person.date = req.body.date;
            person.amounts = req.body.amounts;
            person.name = req.body.name;
            person.price = req.body.price;
            person.sale = req.body.sale;
            person.image = req.body.image;
            person.image_src = req.body.image_src;
            person.detail = req.body.detail;
            person.gift = req.body.gift;
            person.gifts = req.body.gifts;
            person.promotion = req.body.promotion;
            person.promotions = req.body.promotions;
            person.pk = req.body.pk;
            person.guarantee = req.body.guarantee;
            person.guarantees = req.body.guarantees;

            person.manufacturer = req.body.manufacturer;
            person.color = req.body.color;
            person.screen = req.body.screen;
            person.ram = req.body.ram;
            person.rom = req.body.rom;
            person.cpu = req.body.cpu;
            person.pin = req.body.pin;
            person.waterproof = req.body.waterproof;
            person.material = req.body.material;
            person.description = req.body.description;

            person.save().then(business => {
                res.json('Update complete');
            })
                .catch(err => {
                    res.status(400).send("unable to update the database");
                });
        }
    });
});

// Defined delete | remove | destroy route
personRoutes.route('/delete/:id').get(function (req, res) {
    Person.findByIdAndRemove({ _id: req.params.id }, function (err, person) {
        if (err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = personRoutes;
