const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Orders = new Schema({
    orderCheck: {
        type: Boolean
    },
    nickname: {
        type: String
    },
    phone: {
        type: Number
    },
    email: {
        type: String
    },
    address: {
        type: String
    },
    note: {
        type: String
    },
    name: {
        type: String
    },
    amount: {
        type: Number
    },
    price: {
        type: Number
    },
    sale: {
        type: Number
    },
    color: {
        type: String
    },
    advisory: {
        type: Number
    },
    id: {
        type: String
    },
    image: {
        type: String
    },
    date: {
        type: Date
    },
    totalPrice: {
        type: Number
    }
}, {
    collection: 'orders'
});

module.exports = mongoose.model('Orders', Orders);
