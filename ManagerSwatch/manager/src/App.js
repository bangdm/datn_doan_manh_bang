import React, { Component } from 'react';
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import swal from '@sweetalert/with-react';

import Login from './components/auths/login';
import Register from './components/auths/register';
import Profile from './components/auths/proFile';
import Create from './components/managerProducts/create.component';
import Index from './components/managerProducts/index.component';
import Edit from './components/managerProducts/edit.component';
import ShowCart from './components/shoppingcart/Showcart';
import Editcart from './components/shoppingcart/Editcart';
import ShowCartSup from './components/support/Showcart';
import NotFound from './components/layer/Notfound.component';

import AdminHome from './components/Home/adminHome';
import Bill from './components/shoppingcart/bills/bill';
import Home from './components/Home/home';
import Footer from './components/layer/Footer';
import Hello from './components/Home/helloAdmin';
import Searchorder from './components/shoppingcart/searchOrder';

export default class App extends Component {
    logOut = () => {
        localStorage.removeItem('usertoken');
        // window.location.reload();
        swal("Bạn đã đăng xuất thành công.", "", "success");
        setTimeout(() => {
            window.location.reload();
        }, 2000)
    };
    render() {
        return (
            <div>
                <Router>
                    <div id="viewport">
                        <div id="sidebar">
                            <header>
                                {localStorage.getItem('usertoken') === null ?
                                    <img src="./Images/LOGO_swatch_luxury.png" style={{ width: "80%" }} className="img-responsive" alt="LogoSwatch" /> :
                                    <Home />
                                }
                            </header>
                            <ul className="nav">
                                <li>
                                    <Link className="nav-link text-white" to="/">TRANG CHỦ</Link>
                                </li>
                                <li>
                                    {localStorage.getItem('usertoken') === null ?
                                        <Link className="nav-link text-dark" to="" onClick={this.onClick = () => swal("Chưa đăng nhập.", "", "warning")}>THÊM SẢN PHẨM MỚI</Link> :
                                        <Link className="nav-link text-white" to="/create">THÊM SẢN PHẨM MỚI</Link>
                                    }
                                </li>
                                <li>
                                    {localStorage.getItem('usertoken') === null ?
                                        <Link className="nav-link text-dark" to="" onClick={this.onClick = () => swal("Chưa đăng nhập.", "", "warning")}>QUẢN LÝ DS SẢN PHẨM</Link> :
                                        <Link className="nav-link text-white" to="/index">QUẢN LÝ DS SẢN PHẨM</Link>
                                    }
                                </li>
                                <li>
                                    {localStorage.getItem('usertoken') === null ?
                                        <Link className="nav-link text-dark" to="" onClick={this.onClick = () => swal("Chưa đăng nhập.", "", "warning")}>QUẢN LÝ ĐƠN SẢN PHẨM</Link> :
                                        <Link className="nav-link text-white" to="/showcart">QUẢN LÝ ĐƠN SẢN PHẨM</Link>
                                    }
                                </li>
                                <li>
                                    {localStorage.getItem('usertoken') === null ?
                                        <Link className="nav-link text-dark" to="" onClick={this.onClick = () => swal("Chưa đăng nhập.", "", "warning")}>QUẢN LÝ YÊU CẦU HỖ TRỢ</Link> :
                                        <Link className="nav-link text-white" to="/support">QUẢN LÝ YÊU CẦU HỖ TRỢ</Link>
                                    }
                                </li>
                                <li>
                                    {localStorage.getItem('usertoken') === null ?
                                        <Link className="nav-link text-dark" to="" onClick={this.onClick = () => swal("Chưa đăng nhập.", "", "warning")}>QUẢN LÝ TÀI KHOẢN ADMIN</Link> :
                                        <Link className="nav-link text-white" to="/account">QUẢN LÝ TÀI KHOẢN ADMIN</Link>
                                    }
                                </li>
                                <li>
                                    {localStorage.getItem('usertoken') === null ?
                                        <Link to="/" className="nav-link text-white">ĐĂNG NHẬP</Link> :
                                        <Link to="" className="nav-link text-dark disabled">ĐĂNG NHẬP</Link>
                                    }
                                </li>
                                <li style={{ marginTop: '100%', marginLeft: '22%' }}>
                                    {localStorage.getItem('usertoken') === null ?
                                        <Link className="nav-link text-dark disabled" to="">Đăng xuất</Link> :
                                        <Link className="dropdown-item" to="/" onClick={this.logOut}>Đăng xuất</Link>
                                    }
                                </li>
                            </ul>
                        </div>

                        <div id="content" className="container-fluid">
                            <div className="navbar navbar-light bg-secondary">
                                <div className="col-8">
                                    <img src="./Images/LOGO_swatch_luxury.png" style={{ width: "15%" }} className="img-responsive" alt="LogoSwatch" />
                                </div>
                                <div className="col-4" style={{ textAlign: 'right' }}>
                                    <b className="navbar-brand">
                                        {localStorage.getItem('usertoken') === null ?
                                            <p>Chưa đăng nhập</p> :
                                            <span>Hi <b><Hello /></b></span>
                                        }
                                    </b>
                                </div>
                            </div><br />
                            <div>
                                <div className="jumbotron mt-1">
                                    <Switch>
                                        <Route path="/homes" component={AdminHome} />
                                        <Route exact path="/" component={Login} />
                                        <Route path="/register" component={Register} />
                                        <Route path="/profile" component={Profile} />
                                        <Route path="/create" component={Create} />
                                        <Route path="/index" component={Index} />
                                        <Route path="/edit/:id" component={Edit} />
                                        <Route path="/showcart" component={ShowCart} />
                                        <Route path="/editcart/:id" component={Editcart} />
                                        <Route path="/support" component={ShowCartSup} />
                                        <Route path="/print/:id" component={Bill} />
                                        <Route path="/search/order" component={Searchorder} />
                                        <Route component={NotFound} />
                                    </Switch>
                                </div>
                            </div>
                            <Footer />
                        </div>
                    </div>
                </Router>
            </div>
        )
    }
}
