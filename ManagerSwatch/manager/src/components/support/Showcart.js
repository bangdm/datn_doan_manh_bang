import React, { Component } from 'react';
import axios from 'axios';
import Tablecart from '../support/Tablecart';
import { Redirect, Link } from 'react-router-dom';

export default class Showcart extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem('usertoken');
        let loggedIn = true;
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            supports: [],
            loggedIn
        };
    }

    componentDidMount() {
        axios.get('http://localhost:4000/supports')
            .then(response => {
                // console.log(response.data);
                this.setState({ supports: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tabRow() {
        return this.state.supports.map(function (object, i) {
            return <Tablecart obj={object} key={i} />;
        });
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        return (
            <div className="container">
                <div className="container mx-auto row">
                    <div className="col-12">
                        <h3 className="text-center"><strong>Hỗ trợ khách hàng</strong></h3>
                        <table className="table table-striped text-center" style={{ marginTop: 20 }}>
                            <thead>
                                <tr className="bg-secondary">
                                    <th>Số điện thoại</th>
                                    <th>Thời gian</th>
                                    <th className="text-center">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.supports.length > 0 ? this.tabRow() : <h4>KHÔNG CÓ YÊU CẦU.</h4>}
                            </tbody>
                        </table><br />
                        <Link to="/" className="btn btn-sm btn-danger text-left">Quay lại</Link>
                    </div>
                </div>
            </div>
        );
    }
}
