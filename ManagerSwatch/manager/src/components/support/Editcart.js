import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

export default class Editcart extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem('usertoken');
        let loggedIn = true;
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            nickname: '',
            phone: '',
            email: '',
            address: '',
            note: '',

            name: '',
            amount: '',
            color: '',
            price: '',
            sale: '',
            _id: '',

            loggedIn
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/orders/edit/' + this.props.match.params.id)
            .then(res => {
                this.setState({
                    nickname: res.data.nickname,
                    phone: res.data.phone,
                    email: res.data.email,
                    address: res.data.address,
                    note: res.data.note,

                    name: res.data.name,
                    amount: res.data.amount,
                    color: res.data.color,
                    price: res.data.price,
                    sale: res.data.sale,
                    _id: res.data._id
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangeNickname = (e) => {
        this.setState({
            nickname: e.target.value
        });
    }
    onChangePhone = (e) => {
        this.setState({
            phone: e.target.value
        })
    }
    onChangeEmail = (e) => {
        this.setState({
            email: e.target.value
        })
    }
    onChangeAddress = (e) => {
        this.setState({
            address: e.target.value
        })
    }
    onChangeNote = (e) => {
        this.setState({
            note: e.target.value
        })
    }

    onChangeId = (e) => {
        this.setState({
            _id: e.target.value
        })
    }
    onChangeName = (e) => {
        this.setState({
            name: e.target.value
        })
    }
    onChangeColor = (e) => {
        this.setState({
            color: e.target.value
        })
    }
    onChangePrice = (e) => {
        this.setState({
            price: e.target.value
        })
    }
    onChangeSale = (e) => {
        this.setState({
            sale: e.target.value
        })
    }
    onChangeAmount = (e) => {
        this.setState({
            amount: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        const obj = {
            nickname: this.state.nickname,
            phone: this.state.phone,
            email: this.state.email,
            address: this.state.address,
            note: this.state.note,

            name: this.state.name,
            amount: this.state.amount,
            color: this.state.color,
            price: this.state.price,
            sale: this.state.sale,
            _id: this.state._id
        };
        axios.post('http://localhost:4000/orders/update/' + this.props.match.params.id, obj)
            .then(res => console.log(res.data));

        this.props.history.push('/showcart');
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        return (
            <div style={{ marginTop: 10 }} className="container bg-secondary jumbotron mt-5">
                <h1 align="center" className="text-white">Cập nhật lại thông tin khách hàng</h1>
                <form onSubmit={this.onSubmit}>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group">
                                <label className="text-white">(*) HỌ & TÊN</label>
                                <input type="text" className="form-control" placeholder="..."
                                    value={this.state.nickname}
                                    onChange={this.onChangeNickname} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">(*) SỐ ĐIỆN THOẠI</label>
                                <input type="text" className="form-control" placeholder="+84"
                                    value={this.state.phone}
                                    onChange={this.onChangePhone} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">() EMAIL</label>
                                <input type="text" className="form-control" placeholder="...@email.com.vn"
                                    value={this.state.email}
                                    onChange={this.onChangeEmail} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">(*) ĐỊA CHỈ</label>
                                <input type="text" className="form-control" placeholder="số nhà / đường / ấp / xã / huyện / tỉnh "
                                    value={this.state.address}
                                    onChange={this.onChangeAddress} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">() GHI CHÚ</label>
                                <input type="text" className="form-control" placeholder="..."
                                    value={this.state.note}
                                    onChange={this.onChangeNote} />
                            </div>
                        </div>

                        <div className="col-6">
                            <div className="form-group">
                                <label className="text-white">Id name</label>
                                <input type="text" className="form-control" placeholder="..."
                                    value={this.state._id}
                                    onChange={this.onChangeId} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">Name</label>
                                <input type="text" className="form-control" placeholder="..."
                                    value={this.state.name}
                                    onChange={this.onChangeName} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">Amount</label>
                                <input type="number" className="form-control" placeholder="..."
                                    min="1" max="4"
                                    value={this.state.amount}
                                    onChange={this.onChangeAmount} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">Color</label>
                                <input type="text" className="form-control" placeholder="..."
                                    value={this.state.color}
                                    onChange={this.onChangeColor} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">Price</label>
                                <input type="number" className="form-control" placeholder="..."
                                    value={this.state.price}
                                    onChange={this.onChangePrice} />
                            </div>
                            <div className="form-group">
                                <label className="text-white">Sale</label>
                                <input type="number" className="form-control" placeholder="..."
                                    value={this.state.sale}
                                    onChange={this.onChangeSale} />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <input type="submit"
                            value="Update Order"
                            className="btn btn-primary btn-lg btn-block" />
                    </div>
                    <p className="text-danger">Lưu ý: (*) bắt buộc. () không bắt buộc.</p>
                </form>
            </div>
        )
    }
}
