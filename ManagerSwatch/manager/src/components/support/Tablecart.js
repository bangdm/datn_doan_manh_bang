import React, { Component } from 'react';
import axios from 'axios';
import swal from '@sweetalert/with-react';

export default class Tablecart extends Component {
    delete = (e) => {
        axios.get('http://localhost:4000/supports/delete/' + this.props.obj._id)
            .then(swal("Bạn đã xóa thành công.", "", "success"))
            .then(setTimeout(() => {
                window.location.reload()
            }, 2000))
            .catch(err => console.log(err))
    }

    render() {
        const { advisory, date } = this.props.obj;
        return (
            <tr>
                <td>
                    <strong> {advisory} </strong>
                </td>
                <td>
                    <strong> {date} </strong>
                </td>
                <td>
                    <button onClick={this.delete} className="btn btn-sm btn-danger">Xóa</button>
                </td>
            </tr>
        );
    }
}
