import React, { Component } from 'react';
import { Link, Redirect } from "react-router-dom";
import axios from 'axios';
import swal from '@sweetalert/with-react';

export default class Searchorder extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem('usertoken');
        let loggedIn = true;
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            orders: [],
            visible: 8,
            error: false,
            query: "",
            filteredData: [],

            loggedIn,

            check: '',
            status: '',
            date: '',
            amounts: 0,
            name: '',
            price: '',
            sale: '',
            image: '',
            image_src: '',
            detail: '',
            gift: '',
            gifts: '',
            promotion: '',
            promotions: '',
            pk: '',
            guarantee: '',
            guarantees: '',

            manufacturer: '',
            color: '',
            screen: '',
            ram: '',
            rom: '',
            cpu: '',
            pin: '',
            waterproof: '',
            material: '',
            persons: []
        };
    };

    loadMore = () => {
        this.setState((prev) => {
            return { visible: prev.visible + 4 };
        });
    }

    handleInputChange = event => {
        const query = event.target.value;

        this.setState(prevState => {
            const filteredData = prevState.orders.filter(element => {
                return element.nickname.toLowerCase().includes(query.toLowerCase());
            });

            return {
                query,
                filteredData
            };
        });
    };

    getData = () => {
        fetch(`http://localhost:4000/orders`)
            .then(response => response.json())
            .then(orders => {
                const { query } = this.state;
                const filteredData = orders.filter(element => {
                    return element.nickname.toLowerCase().includes(query.toLowerCase());
                });

                this.setState({
                    orders,
                    filteredData
                });
            });
    };

    componentWillMount() {
        this.getData();
    }


    //update data
    onhandleUpdate = (e) => {
        this.setState({
            amounts: e.target.value
        })
    }

    componentDidMount() {
        axios.get('http://localhost:4000/persons/detail/' + this.state.orders.id)
            .then(res => {
                this.setState({
                    _id: res.data._id,
                    name: res.data.name,
                    status: res.data.status,
                    check: res.data.check,
                    amounts: res.data.amounts,
                    price: res.data.price,
                    image: res.data.image,
                    sale: res.data.sale,
                    color: res.data.color,
                    color1: res.data.color1,
                    color2: res.data.color2,
                    gift: res.data.gift,
                    gifts: res.data.gifts,
                    promotionone: res.data.promotionone,
                    promotiontwo: res.data.promotiontwo,
                    manufacturer: res.data.manufacturer,
                    screen: res.data.screen,
                    pin: res.data.pin,
                    ram: res.data.ram,
                    rom: res.data.rom,
                    cpu: res.data.cpu,
                    pk: res.data.pk,
                    waterproof: res.data.waterproof,
                    material: res.data.material,
                    guarantee: res.data.guarantee,
                    guarantees: res.data.guarantees,
                    image_src: res.data.image_src,
                    detail: res.data.detail,
                    video: res.data.video,
                    description: res.data.description
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }


    delete = (e) => {
        axios.get('http://localhost:4000/orders/delete/' + this.element._id)
            .then(swal("Bạn đã hủy thành công đơn hàng.", `Mã đơn: ${this.element._id}`, "success"))
            // .then(setTimeout(() => {
            //     window.location.reload()
            // }, 2500))
            .catch(err => console.log(err))

        const updateData = {
            check: this.state.check,
            status: this.state.status,
            date: this.state.date,
            amounts: this.state.amounts,
            name: this.state.name,
            price: this.state.price,
            sale: this.state.sale,
            image: this.state.image,
            image_src: this.state.image_src,
            detail: this.state.detail,
            gift: this.state.gift,
            gifts: this.state.gifts,
            promotion: this.state.promotion,
            promotions: this.state.promotions,
            pk: this.state.pk,
            guarantee: this.state.guarantee,
            guarantees: this.state.guarantees,

            manufacturer: this.state.manufacturer,
            color: this.state.color,
            screen: this.state.screen,
            ram: this.state.ram,
            rom: this.state.rom,
            cpu: this.state.cpu,
            pin: this.state.pin,
            waterproof: this.state.waterproof,
            material: this.state.material
        };
        axios.post('http://localhost:4000/persons/update/' + this.state.id, updateData)
            .then(res => console.log(res.data))
            .then(() => {
                swal("Bạn đã cập nhật thành công.", "", "success");
                setTimeout(() => {
                    window.location.reload();
                }, 1000)
            })
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }

        // const y = this.state.persons;
        // const x = this.state.orders;
        // const e = x.map((e, i) => {
        //     return e.amount
        // })
        // const ys = y.map((e, i) => {
        //     return e.amounts
        // })
        // console.log(ys + e)
        return (
            <div className="containers">
                <form className="ml-md-3">
                    <div className="form-group text-left border-top border-bottom p-2 mt-2 mb-2 row">
                        <label htmlFor="inputName" className="col-sm-auto col-form-label">Nhập tên khách hàng: </label>
                        <div className="col-sm-9">
                            <input type="name"
                                className="form-control"
                                autoComplete="on" autoFocus
                                id="inputName"
                                placeholder="Search ..."
                                value={this.state.query}
                                onChange={this.handleInputChange}
                            />
                        </div>
                    </div>
                </form>

                <div>
                    <table className="table table-striped" style={{ marginTop: 20 }}>
                        <thead>
                            <tr className="bg-secondary">
                                <th>Tên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Địa chỉ</th>
                                <th>Ghi chú</th>
                                <th>Thời gian</th>
                                <th>Chi tiết</th>
                                <th>Hành động</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.filteredData.slice(0, this.state.visible).map((element, index) =>
                                <tr key={index}>
                                    <td>
                                        <strong> {element.nickname} </strong>
                                    </td>
                                    <td>
                                        {element.phone}
                                    </td>
                                    <td>
                                        {element.email}
                                    </td>
                                    <td>
                                        {element.address}
                                    </td>
                                    <td>
                                        {element.note}
                                    </td>
                                    <td>
                                        {element.date}
                                    </td>
                                    <td>
                                        <div className="btn-group dropright">
                                            <button type="button" className="btn btn-sm  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Chi tiết
                                        </button>
                                            <div className="dropdown-menu text-center">
                                                <img style={{ width: 100, height: 100 }} src={element.image} alt={element.name} />
                                                <h6><b>{element.name}</b></h6>
                                                <div className="dropdown-divider"></div>
                                                <h6>Price: <del>{(element.price).toLocaleString()}</del><sup>vnđ</sup></h6>
                                                <h6 className="text-danger">Sale: {(element.sale).toLocaleString()}<sup>vnđ</sup></h6>
                                                <h6>Color: <b>{element.color}</b></h6>
                                                <h6>Amount: <b>{element.amount}</b></h6>
                                                <h6>Total_Pay: <b className="text-danger">{(element.sale * element.amount).toLocaleString()}</b> vnđ.</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <Link to={"/editcart/" + element._id} className="btn btn-sm btn-primary">Cập nhật</Link> &nbsp;
                                        {element.orderCheck ?
                                            <Link to={"/print/" + element._id} className="btn btn-sm btn-success">In đơn hàng</Link> :
                                            <button onClick={this.delete} value={this.state.amounts} onChange={this.onhandleUpdate} className="btn btn-sm btn-danger">Hủy đơn hàng</button>
                                        }

                                    </td>
                                    <td>
                                        <div className="form-group">
                                            <label className="form-check-label">
                                                {element.orderCheck ?
                                                    <b className="bg-success">Đã duyệt</b> :
                                                    <b className="bg-warning">Đang chờ</b>
                                                }
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </table><br />
                </div>
                <div className="text-center p-3">
                    {this.state.visible < this.state.orders.length &&
                        <button onClick={this.loadMore} type="button" className="load-more btn btn-success">Hiện thị thêm</button>
                    }
                </div>
            </div>
        )
    }
}
