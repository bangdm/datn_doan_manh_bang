import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import swal from '@sweetalert/with-react';

export default class Tablecart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            check: '',
            status: '',
            date: '',
            amounts: 0,
            name: '',
            price: '',
            sale: '',
            image: '',
            image_src: '',
            detail: '',
            gift: '',
            gifts: '',
            promotion: '',
            promotions: '',
            pk: '',
            guarantee: '',
            guarantees: '',

            manufacturer: '',
            color: '',
            screen: '',
            ram: '',
            rom: '',
            cpu: '',
            pin: '',
            waterproof: '',
            material: '',
            persons: []
        }
    }

    onhandleUpdate = (e) => {
        this.setState({
            amounts: e.target.value
        })
    }

    componentDidMount() {
        axios.get('http://localhost:4000/persons/detail/' + this.props.obj.id)
            .then(res => {
                this.setState({
                    _id: res.data._id,
                    name: res.data.name,
                    status: res.data.status,
                    check: res.data.check,
                    amounts: res.data.amounts,
                    price: res.data.price,
                    image: res.data.image,
                    sale: res.data.sale,
                    color: res.data.color,
                    color1: res.data.color1,
                    color2: res.data.color2,
                    gift: res.data.gift,
                    gifts: res.data.gifts,
                    promotionone: res.data.promotionone,
                    promotiontwo: res.data.promotiontwo,
                    manufacturer: res.data.manufacturer,
                    screen: res.data.screen,
                    pin: res.data.pin,
                    ram: res.data.ram,
                    rom: res.data.rom,
                    cpu: res.data.cpu,
                    pk: res.data.pk,
                    waterproof: res.data.waterproof,
                    material: res.data.material,
                    guarantee: res.data.guarantee,
                    guarantees: res.data.guarantees,
                    image_src: res.data.image_src,
                    detail: res.data.detail,
                    video: res.data.video,
                    description: res.data.description
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }


    delete = (e) => {
        axios.get('http://localhost:4000/orders/delete/' + this.props.obj._id)
            .then(swal("Bạn đã hủy thành công đơn hàng.", `Mã đơn: ${this.props.obj._id}`, "success"))
            // .then(setTimeout(() => {
            //     window.location.reload()
            // }, 2500))
            .catch(err => console.log(err))

        const updateData = {
            check: this.state.check,
            status: this.state.status,
            date: this.state.date,
            amounts: this.state.amounts,
            name: this.state.name,
            price: this.state.price,
            sale: this.state.sale,
            image: this.state.image,
            image_src: this.state.image_src,
            detail: this.state.detail,
            gift: this.state.gift,
            gifts: this.state.gifts,
            promotion: this.state.promotion,
            promotions: this.state.promotions,
            pk: this.state.pk,
            guarantee: this.state.guarantee,
            guarantees: this.state.guarantees,

            manufacturer: this.state.manufacturer,
            color: this.state.color,
            screen: this.state.screen,
            ram: this.state.ram,
            rom: this.state.rom,
            cpu: this.state.cpu,
            pin: this.state.pin,
            waterproof: this.state.waterproof,
            material: this.state.material
        };
        axios.post('http://localhost:4000/persons/update/' + this.props.obj.id, updateData)
            .then(res => console.log(res.data))
            .then(() => {
                swal("Bạn đã hủy thành công đơn hàng.", `Mã đơn: ${this.props.obj._id}`, "success");
                setTimeout(() => {
                    window.location.reload();
                }, 1000)
            })
    }

    render() {
        const { nickname, phone, email, address, note, date, amount, name, color, image, price, sale, orderCheck } = this.props.obj;
        const total = sale * amount;
        const updateAmount = this.state.amounts + amount;
        return (
            <tr>
                <td>
                    <strong> {nickname} </strong>
                </td>
                <td>
                    {phone}
                </td>
                <td>
                    {email}
                </td>
                <td>
                    {address}
                </td>
                <td>
                    {note}
                </td>
                <td>
                    {date}
                </td>
                <td>
                    <div className="btn-group dropright">
                        <button type="button" className="btn btn-sm  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Chi tiết
                        </button>
                        <div className="dropdown-menu text-center">
                            <img style={{ width: 100, height: 100 }} src={image} alt={name} />
                            <h6><b>{name}</b></h6>
                            <div className="dropdown-divider"></div>
                            <h6>Price: <del>{(price).toLocaleString()}</del><sup>vnđ</sup></h6>
                            <h6 className="text-danger">Sale: {(sale).toLocaleString()}<sup>vnđ</sup></h6>
                            <h6>Color: <b>{color}</b></h6>
                            <h6>Amount: <b>{amount}</b></h6>
                            <h6>Total_Pay: <b className="text-danger">{(total).toLocaleString()}</b> vnđ.</h6>
                        </div>
                    </div>
                </td>
                <td>
                    <Link to={"/editcart/" + this.props.obj._id} className="btn btn-sm btn-primary">Cập nhật</Link> &nbsp;
                    {orderCheck ?
                        <Link to={"/print/" + this.props.obj._id} className="btn btn-sm btn-success">In đơn hàng</Link> :
                        <button onClick={this.delete} value={this.state.amounts = updateAmount} onChange={this.onhandleUpdate} className="btn btn-sm btn-danger">Hủy đơn hàng</button>
                    }
                </td>
                <td>
                    <div className="form-group">
                        <label className="form-check-label">
                            {orderCheck ?
                                <b className="bg-success">Đã duyệt</b> :
                                <b className="bg-warning">Đang chờ</b>
                            }
                        </label>
                    </div>
                </td>
            </tr>
        );
    }
}
