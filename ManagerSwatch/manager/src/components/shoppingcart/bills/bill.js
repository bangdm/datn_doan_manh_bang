import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import ReactToPrint, { PrintContextConsumer } from 'react-to-print';

export default class Bill extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem('usertoken');
        let loggedIn = true;
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            orders: [],
            orderCheck: false,
            nickname: '',
            phone: '',
            email: '',
            address: '',
            note: '',

            name: '',
            image: '',
            amount: '',
            color: '',
            price: '',
            sale: '',
            _id: '',

            loggedIn
        }
    }

    componentDidMount() {
        axios.get('http://localhost:4000/orders/print/' + this.props.match.params.id)
            .then(res => {
                this.setState({
                    orderCheck: res.data.orderCheck,
                    nickname: res.data.nickname,
                    phone: res.data.phone,
                    email: res.data.email,
                    address: res.data.address,
                    note: res.data.note,
                    date: res.data.date,

                    name: res.data.name,
                    amount: res.data.amount,
                    image: res.data.image,
                    color: res.data.color,
                    price: res.data.price,
                    sale: res.data.sale,
                    _id: res.data._id
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        const { nickname, phone, email, address, note, date, name, image, price, sale, color, amount } = this.state;
        return (
            <div className="container">
                <div className="containers" ref={el => (this.componentRef = el)}><br />
                    <div>
                        <span>- Địa chỉ: <b>Số 24 - Nguyễn Cơ Trạch - Mỹ Đình 1 - Hà Nội.</b></span>
                        <p>- Liên hệ: <b>0978.439.197</b></p>
                    </div>
                    <h2 className="text-center"><b>ĐƠN THANH TOÁN</b></h2><br />
                    <div className="row border">
                        <div className="col-4 border">
                            <h4 className="text-center">Thông tin khách hàng</h4><br />
                            <p>Tên khách hàng: <b>{nickname}</b></p>
                            <p>Số điện thoại: <b>{phone}</b></p>
                            <p>Email: <b>{email}</b></p>
                            <p>Địa chỉ: <b>{address}</b></p>
                            <p>Ghi chú: <b>{note}</b></p>
                            <p>Thời gian: <b>{date}</b>{}</p>
                        </div>
                        <div className="col-8">
                            <h4 className="text-center">Thông tin sản phẩm</h4><br />
                            <div className="row">
                                <div className="col-4">
                                    <img src={image} alt={name}
                                        height={image ? 190 : 0}
                                        width={image ? 190 : 0}
                                    />
                                </div>
                                <div className="col-8">
                                    <p>Tên sản phẩm: <b>{name}</b></p>
                                    <p>Giá: <b>{(price).toLocaleString()}</b> vnđ.</p>
                                    <p>Giảm giá: <b>{(sale).toLocaleString()}</b> vnđ.</p>
                                    <p>Số lượng: <b>{amount}</b></p>
                                    <p>Màu sắc: <button className="btn btn-bg" style={{ backgroundColor: color }} /> <b>{color}</b></p>
                                </div>
                            </div>
                        </div>
                    </div> &nbsp;
                <div className="row">
                        <div className="col-8">
                            <span>Shop bán lẻ đồng hồ thông minh Swatch.com.vn cảm ơn quý khách đã mua hàng.</span> <br />
                            <span>Swatch hân hạnh được phục vụ quý khách.</span>
                        </div>
                        <div className="col-4 border text-center">
                            <h6>Tổng giá:  <b className="text-danger">{(sale * amount).toLocaleString()}</b> vnđ.</h6>
                            <h4>Thanh toán:  <b className="text-danger">{(sale * amount).toLocaleString()}</b> vnđ.</h4>
                        </div>
                    </div><br />
                </div>
                <div className="text-right">
                    <ReactToPrint content={() => this.componentRef}>
                        <PrintContextConsumer>
                            {({ handlePrint }) => (
                                <button className="btn btn-bg btn-success" onClick={handlePrint}>IN HÓA ĐƠN >></button>
                            )}
                        </PrintContextConsumer>
                    </ReactToPrint>
                </div>
            </div>
        )
    }
}
