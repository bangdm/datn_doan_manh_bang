import React, { Component } from 'react';
import axios from 'axios';
import Tablecart from '../shoppingcart/Tablecart';
import { Redirect } from 'react-router-dom';

export default class Showcart extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem('usertoken');
        let loggedIn = true;
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            orders: [],
            orderCheck: true,
            loggedIn
        };
    }

    componentDidMount() {
        axios.get('http://localhost:4000/orders')
            .then(response => {
                // console.log(response.data);
                this.setState({ orders: response.data });
            })
            .catch(error => {
                console.log(error);
            })
    }

    tabRow() {
        return this.state.orders.map(function (object, i) {
            return <Tablecart obj={object} key={i} />;
        });
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        const bills = Object.values(this.state.orders).length;

        const orderWarning = this.state.orders.filter((item) => {
            return item.orderCheck === false
        })
        const orderApproved = this.state.orders.filter((item) => {
            return item.orderCheck === true
        })
        return (
            <div>
                <div className="containers">
                    <div className="containers" style={{ overflow: "auto" }}>
                        <h3 className="text-center"><strong>DANH SÁCH ĐƠN HÀNG</strong></h3>
                        <div className="row">
                            <div className="col-12">
                                <p>- Tổng số lượng đơn hàng: <b>{bills}</b></p>
                                <p>- Tổng đơn đã duyệt: <b>{orderApproved.length}</b></p>
                                <p>- Tổng đơn đang chờ: <b>{orderWarning.length}</b></p>
                            </div>
                        </div>
                        <table className="table table-striped" style={{ marginTop: 20 }}>
                            <thead>
                                <tr className="bg-secondary">
                                    <th>Tên</th>
                                    <th>Số điện thoại</th>
                                    <th>Email</th>
                                    <th>Địa chỉ</th>
                                    <th>Ghi chú</th>
                                    <th>Thời gian</th>
                                    <th>Chi tiết</th>
                                    <th>Hành động</th>
                                    <th>Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.tabRow()}
                            </tbody>
                        </table><br />
                    </div>
                </div>
            </div>
        );
    }
}
