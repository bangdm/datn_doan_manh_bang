import React from 'react';
import './editlayer/Footer.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretSquareUp } from '@fortawesome/free-solid-svg-icons';

class Footer extends React.Component {
	render() {
		const Caretsquareup = <FontAwesomeIcon icon={faCaretSquareUp} />
		return (
			<div>
				<div className="bg-dark">
					<div className="container bg-dark">
						<div className="row">
							<p className="text-white text-center"><small>© 2020 Shop Bán Lẻ SWATCH / Địa chỉ: 24 Nguyễn Cơ Trạch, Mỹ Đình, Từ Liêm, TP. Hà Nội / GPĐKKD số 06868686 do Sở KHĐT TP.HN cấp ngày 08/03/2012. GP số 47/GP-TTĐT do sở TTTT TP HCM cấp ngày 01/01/2020. Điện thoại: (098)8888888. Email: smartwatch@gmail.com.vn. Chịu trách nhiệm nội dung: DoanManhBang.</small></p>
						</div>
					</div>
					<div className="bg-secondary back-to-top">{Caretsquareup}</div>
				</div>
			</div>
		)
	}
}

export default Footer;
