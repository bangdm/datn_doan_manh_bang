import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import jwt_decode from 'jwt-decode';

export default class Menu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            errors: {},
        }
    }

    componentDidMount(e) {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            first_name: decoded.first_name,
            last_name: decoded.last_name,
            email: decoded.email,
            loggedIn: true
        })
    }

    logOut = () => {
        localStorage.removeItem('usertoken');
        alert('Logged out successfully');
        this.props.history.push(`/`);
    };

    render() {
        const { email } = this.state;
        return (
            <div className="container bg-secondary">
                <img src="./Images/LOGO_swatch_luxury.png" style={{ width: "15%" }} className="img-responsive" alt="LogoSwatch" />
                <nav className="navbar navbar-expand-lg navbar-light bg-dark">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item active">
                                <Link className="nav-link text-white" to="/create">Create</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/index">Index</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/showcart">OrderCart</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link disabled" to="">About</Link>
                            </li>
                        </ul>
                        <form className="form-inline my-2 my-lg-0">
                            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
                                <a className="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <strong className="text-white">{email}</strong>
                                </a>
                                <div className="dropdown-menu bg-secondary" aria-labelledby="navbarDropdownMenuLink">
                                    <Link className="dropdown-item" to="/profile">Profile</Link>
                                    <Link className="dropdown-item" to="/register">Register</Link>
                                    <Link className="dropdown-item" to="/" onClick={this.logOut}>Logout</Link>
                                </div>
                            </li>
                        </form>
                    </div>
                </nav>
            </div>
        )
    }
}
