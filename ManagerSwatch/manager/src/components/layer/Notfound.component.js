import React from 'react';

class Notfound extends React.Component {
	render() {
		return (
			<div className="container">
				<hr />
				<br />
				<div className="alert alert-warning">
					<button type="button" className="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong className="text-danger">Không tìm thấy trang !</strong>
				</div>
				<br />
			</div>
		)
	}
}

export default Notfound;
