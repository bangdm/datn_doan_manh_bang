import React from 'react';
import { Link } from 'react-router-dom';

export default class LogFalse extends React.Component {
    render() {
        return (
            <div className="container jumbotron mt-5 text-center">
                <div className="text-center bg-warning"><strong>LOGIN FALSE !</strong></div><br/>
                <Link to="/" className="btn btn-sm btn-danger">Go back</Link>
            </div>
        )
    }
}
