import React, { Component } from 'react';
import jwt_decode from 'jwt-decode';
import { Link, Redirect } from 'react-router-dom';
import swal from '@sweetalert/with-react';

export default class Profile extends Component {
    constructor() {
        super();
        const token = localStorage.getItem('usertoken');

        let loggedIn = true;
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            errors: {},
            loggedIn
        }
    }

    logOut = () => {
        localStorage.removeItem('usertoken');
        // window.location.reload();
        swal("Bạn đã đăng xuất thành công.", "", "success");
        setTimeout(() => {
            window.location.reload();
        }, 2000)
    };

    componentDidMount(e) {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            first_name: decoded.first_name,
            last_name: decoded.last_name,
            email: decoded.email,
            loggedIn: true
        })
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        const { first_name, last_name, email } = this.state;
        return (
            <div className="container">
                <div>
                    <div className="col-sm-8 mx-auto">
                        <h1 className="text-center"><strong>Thông tin tài khoản</strong></h1>
                    </div><br />
                    <table className="table col-md-6 mx-auto">
                        <tbody className="text-center">
                            <tr>
                                <td>Họ</td>
                                <td><strong>{first_name}</strong></td>
                            </tr>
                            <tr>
                                <td>Tên</td>
                                <td><strong>{last_name}</strong></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><strong>{email}</strong></td>
                            </tr>
                        </tbody>
                    </table><br />
                    <div className="row">
                        <div className="col-3"></div>
                        <div className="col-3">
                            <Link to="/" onClick={this.logOut} className="btn btn-lg btn-danger btn-block">
                                Đăng xuất
                        </Link>
                        </div>
                        <div className="col-3">
                            <Link to="/register" className="btn btn-lg btn-success btn-block">
                                Tạo tài khoản mới
                            </Link>
                        </div>
                        <div className="col-3"></div>
                    </div><br />
                </div>
            </div>
        )
    }
}
