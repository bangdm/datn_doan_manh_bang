import React, { Component } from 'react';
import { register } from './userFunction';
import { Redirect } from 'react-router-dom';
import swal from '@sweetalert/with-react';

class Register extends Component {
  constructor() {
    super();
    const token = localStorage.getItem('usertoken')

    let loggedIn = true
    if (token == null) {
      loggedIn = false
    }

    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      errors: {},
      loggedIn
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit = (e) => {
    e.preventDefault();

    if (this.state.first_name === "" || this.state.last_name === "" || this.state.email === "") {
      swal("Vui lòng điền thông tin đầy đủ.", "", "warning");
      return false;
    } else if (this.state.password.length < 6) {
      swal("Mật khẩu dài hơn 6 ký tự", "warning", "warning");
      return false;
    } else {
      swal("Đăng ký thành công.", "Loading...", "success");
    }

    const newUser = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      password: this.state.password
    }

    register(newUser).then(res => {
      this.props.history.push(`/`)
    })
  }

  render() {
    if (this.state.loggedIn === false) {
      return <Redirect to="/" />
    }
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6 mx-auto">
            <h1 className="h3 mb-3 font-weight-normal text-center"><strong>ĐĂNG KÝ TÀI KHOẢN</strong></h1>
            <form noValidate onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="fname">Họ</label>
                <input
                  id="fname"
                  type="text"
                  className="form-control"
                  name="first_name"
                  placeholder="first name"
                  value={this.state.first_name}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="lname">Tên</label>
                <input
                  id="lname"
                  type="text"
                  className="form-control"
                  name="last_name"
                  placeholder="Last name"
                  value={this.state.last_name}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input
                  id="email"
                  type="email"
                  className="form-control"
                  name="email"
                  placeholder="Email"
                  value={this.state.email}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Mật khẩu</label>
                <input
                  id="password"
                  type="password"
                  className="form-control"
                  name="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
              </div><br />
              <button type="submit" className="btn btn-lg btn-primary btn-block">
                Đăng ký
              </button>
            </form><br />
          </div>
        </div>
      </div>
    )
  }
}

export default Register
