import axios from 'axios';

export const register = newUser => {
  return axios
    .post('users/register', {
      first_name: newUser.first_name,
      last_name: newUser.last_name,
      email: newUser.email,
      password: newUser.password
    })
    .then(response => {
      console.log('Registered');
    })
}

export const login = user => {
  return axios
    .post('users/login', {
      email: user.email,
      password: user.password
    })
    .then(response => {
      if (response) {
        localStorage.setItem('usertoken', JSON.stringify(response.data));
        return response.data;
      } else {
        return localStorage.removeItem('usertoken');
      }
    })
    .catch(err => {
      console.log(err);
    })
}

export const getProfile = user => {
  return axios
    .get('users/profile', {
      //headers: { Authorization: ` ${this.getToken()}` }
    })
    .then(response => {
      localStorage.clear();
      console.log(response);
      return response.data;
    })
    .catch(err => {
      console.log(err);
    })
}
