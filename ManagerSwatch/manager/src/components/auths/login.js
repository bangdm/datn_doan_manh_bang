import React, { Component } from 'react';
import './login.css';
import { login } from './userFunction';
import { Link, Redirect } from 'react-router-dom';
import swal from '@sweetalert/with-react';

export default class Login extends Component {
    constructor() {
        super()
        const token = localStorage.getItem('usertoken')

        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            email: '',
            password: '',
            loggedIn
        }
    }

    onHandleClick = () => {
        if (localStorage.getItem('usertoken') === null) {
            setTimeout(() => {
                return swal("Thông tin tài khoản hoặc mật khẩu không chính xác.", "Vui lòng kiểm tra lại", "warning");
            }, 1500)
        }
    }

    regAlert = () => {
        swal("Admin có quyền cấp tài khoản.", "Vui lòng liên hệ admin 0978.439.197", "warning");
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit = (e) => {
        e.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password
        };
        login(user)
            .then(res => {
                if (res) {
                    this.setState({
                        loggedIn: true
                    });
                }
                swal("Đăng nhập thành công.", "", "success");
            })
            .catch(err => {
                return this.props.history.push('/')
            })

    }

    render() {
        if (this.state.loggedIn) {
            setTimeout(() => {
                window.location.reload();
            }, 1000);
            return <Redirect to="/homes" />
        }
        return (
            <div className="container jumbotron mt-5">
                <div className="row">
                    <div className="col-md-6 mt-5 mx-auto">
                        <h1 className="h3 mb-3 font-weight-normal text-center"><strong>ĐĂNG NHẬP TÀI KHOẢN SWATCH</strong></h1>
                        <form onSubmit={this.onSubmit} autoComplete="on">
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input
                                    required autoFocus
                                    autoComplete="off"
                                    id="email"
                                    type="email"
                                    className="form-control"
                                    name="email"
                                    placeholder="Enter email"
                                    value={this.state.email}
                                    onChange={this.onChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Mật khẩu</label>
                                <input
                                    required
                                    id="password"
                                    type="password"
                                    className="form-control"
                                    name="password"
                                    placeholder="Password"
                                    value={this.state.password}
                                    onChange={this.onChange} />
                            </div><br />
                            <div className="row">
                                <div className="col-6">
                                    <button type="submit" className="btn btn-sm btn-primary btn-block" onClick={this.onHandleClick}>
                                        Đăng nhập
                                    </button>
                                </div>
                                <div className="col-6">
                                    <Link to="/" onClick={this.regAlert} className="btn btn-sm btn-success btn-block" id="reg">
                                        Tạo tài khoản
                                    </Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
