import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import Footer from './Footer';

export default class Manager extends React.Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem('usertoken')

        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            loggedIn
        }
    }

    componentDidMount(e) {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            first_name: decoded.first_name,
            last_name: decoded.last_name,
            email: decoded.email,
            loggedIn: true
        })
    }

    logOut = () => {
        localStorage.removeItem('usertoken');
        // this.props.history.push(`/`);
        alert('Logged out successfully');
    };

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        const { email } = this.state;
        return (
            <div className=" bg-secondary">
                <img src="./Images/LOGO_swatch_luxury.png" style={{ width: "15%" }} className="img-responsive" alt="LogoSwatch" />
                <nav className="navbar navbar-expand-lg navbar-light bg-dark">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item active">
                                <Link className="nav-link text-white" to="/create">THÊM SẢN PHẨM MỚI</Link>
                            </li>
                            <span className="text-white">||</span>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/index">QUẢN LÝ DANH SÁCH SẢN PHẨM</Link>
                            </li>
                            <span className="text-white">||</span>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/showcart">QUẢN LÝ ĐƠN HÀNG</Link>
                            </li>
                            <span className="text-white">||</span>
                            <li className="nav-item">
                                <Link className="nav-link text-white" to="/support">QUẢN LÝ YÊU CẦU HỖ TRỢ</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link disabled" to="">About</Link>
                            </li>
                        </ul>
                        <form className="form-inline my-2 my-lg-0">
                            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
                                <a className="nav-link dropdown-toggle text-white" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <strong className="text-white">{email}</strong>
                                </a>
                                <div className="dropdown-menu bg-secondary" aria-labelledby="navbarDropdownMenuLink">
                                    <Link className="dropdown-item" to="/profile">Thông tin tài khoản</Link>
                                    <Link className="dropdown-item" to="/register">Đăng ký</Link>
                                    <Link className="dropdown-item" to="/" onClick={this.logOut}>Đăng xuất</Link>
                                </div>
                            </li>
                        </form>
                    </div>
                </nav>
                <div className="jumbotron mt-5">
                    <h1 className="text-center" style={{ height: "550px" }}>WELCOME MANAGER</h1>
                </div>
                <Footer className="mt-5" />
            </div>
        )
    }
}
