import React from 'react';
import swal from '@sweetalert/with-react';
import './home.css';
import { Redirect } from 'react-router-dom';
import jwt_decode from 'jwt-decode';

export default class Hello extends React.Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem('usertoken')

        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            first_name: '',
            last_name: '',
            email: '',
            loggedIn
        }
    }

    componentDidMount(e) {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            first_name: decoded.first_name,
            last_name: decoded.last_name,
            email: decoded.email,
            loggedIn: true
        })
    }

    logOut = () => {
        localStorage.removeItem('usertoken');
        // window.location.reload();
        swal("Bạn đã đăng xuất thành công.", "", "success");
        setTimeout(() => {
            window.location.reload();
        }, 2000)
    };

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        return (
            <span>
                {this.state.last_name}
            </span>
        )
    }
}
