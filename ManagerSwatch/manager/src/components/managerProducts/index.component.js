import React, { Component } from 'react';
import axios from 'axios';
import TableRow from './TableRow';
import Search from './search';
import { Redirect } from 'react-router-dom';

export default class Index extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem('usertoken');

        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            persons: [],
            totalApple: 'apple',
            totalSamsung: 'samsung',
            totalXiaomi: 'xiaomi',
            totalHuawei: 'huawei',
            filteredApple: [],
            filteredSamsung: [],
            filteredXiaomi: [],
            filteredHuawei: [],
            loggedIn
        };
    }

    //filter totalApple totalSamsung totalXiaomi totalHuawei
    getData = () => {
        fetch(`http://localhost:4000/persons`)
            .then(response => response.json())
            .then(persons => {
                const { totalApple, totalSamsung, totalXiaomi, totalHuawei } = this.state;
                const filteredApple = persons.filter(element => {
                    return element.manufacturer.includes(totalApple);
                });
                const filteredSamsung = persons.filter(element => {
                    return element.manufacturer.includes(totalSamsung);
                });
                const filteredXiaomi = persons.filter(element => {
                    return element.manufacturer.includes(totalXiaomi);
                });
                const filteredHuawei = persons.filter(element => {
                    return element.manufacturer.includes(totalHuawei);
                });

                this.setState({
                    persons,
                    filteredApple,
                    filteredSamsung,
                    filteredXiaomi,
                    filteredHuawei
                });

            });
    };

    componentWillMount() {
        this.getData();
    }

    componentDidMount() {
        axios.get('http://localhost:4000/persons')
            .then(response => {
                // console.log(response.data);
                this.setState({ persons: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tabRow() {
        return this.state.persons.map(function (object, i) {
            return <TableRow obj={object} key={i} />;
        });
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        const totalList = Object.values(this.state.persons).length;
        const appleLenght = this.state.filteredApple.length;
        const samsungLenght = this.state.filteredSamsung.length;
        const xiaomiLenght = this.state.filteredXiaomi.length;
        const huaweiLenght = this.state.filteredHuawei.length;

        const totalAmount = this.state.persons.filter(a => {
            return a.amounts > 0
        })
        const totalAmounts = this.state.persons.filter(a => {
            return a.amounts <= 0
        })
        return (
            <div className="container">
                <h1 align="center"><strong>Danh sách sản phẩm</strong></h1><br />
                <div className="row">
                    <div className="col-12 text-center">
                        <h5>- Tổng sản phẩm: <b>{totalList}</b></h5> &nbsp;
                        <span>- Sản phẩm apple: <b>{appleLenght}</b></span> &nbsp;
                        <span>- Sản phẩm samsung: <b>{samsungLenght}</b></span> &nbsp;
                        <span>- Sản phẩm xiaomi: <b>{xiaomiLenght}</b></span> &nbsp;
                        <span>- Sản phẩm huawei: <b>{huaweiLenght}</b></span> &nbsp;
                        <span>- Sản phẩm còn hàng: <b>{totalAmount.length}</b></span> &nbsp;
                        <span>- Sản phẩm hết hàng: <b>{totalAmounts.length}</b></span> &nbsp;
                    </div>
                </div><hr />
                <table className="table table-striped" style={{ marginTop: 20 }}>
                    <thead>
                        <tr className="bg-dark text-white text-center">
                            <th>Hình ảnh</th>
                            <th>Tên sản phẩm</th>
                            <th>Giá</th>
                            <th>Giảm giá</th>
                            <th>Số lượng</th>
                            <th colSpan="2">Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.tabRow()}
                    </tbody>
                </table>
                <Search />
            </div>
        );
    }
}
