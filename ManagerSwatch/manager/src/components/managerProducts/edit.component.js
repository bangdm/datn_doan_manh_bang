import React, { Component } from 'react';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import swal from '@sweetalert/with-react';

export default class Edit extends Component {
    constructor(props) {
        super(props);
        const token = localStorage.getItem('usertoken');

        let loggedIn = true
        if (token == null) {
            loggedIn = false
        }

        this.state = {
            check: '',
            status: '',
            date: '',
            amounts: '',
            name: '',
            price: '',
            sale: '',
            image: '',
            image_src: '',
            detail: '',
            gift: '',
            gifts: '',
            promotion: '',
            promotions: '',
            pk: '',
            guarantee: '',
            guarantees: '',

            manufacturer: '',
            color: '',
            screen: '',
            ram: '',
            rom: '',
            cpu: '',
            pin: '',
            waterproof: '',
            material: '',
            loggedIn
        }
    }

    //thong tin san pham
    onChangeCheck = (e) => {
        this.setState({
            check: e.target.value
        });
    }
    onChangeStatus = (e) => {
        this.setState({
            status: e.target.value
        });
    }
    onChangeCheck = (e) => {
        this.setState({
            check: e.target.value
        });
    }
    onChangeDate = (e) => {
        this.setState({
            date: e.target.value
        });
    }
    onChangeAmount = (e) => {
        this.setState({
            amounts: e.target.value
        });
    }
    onChangeName = (e) => {
        this.setState({
            name: e.target.value
        });
    }
    onChangePrice = (e) => {
        this.setState({
            price: e.target.value
        });
    }

    onChangeSale = (e) => {
        this.setState({
            sale: e.target.value
        });
    }
    onChangeImage = (e) => {
        this.setState({
            image: e.target.value
        });
    }
    onChangeImage_src = (e) => {
        this.setState({
            image_src: e.target.value
        });
    }
    onChangeDetail = (e) => {
        this.setState({
            detail: e.target.value
        });
    }
    onChangeGift = (e) => {
        this.setState({
            gift: e.target.value
        });
    }
    onChangeGifts = (e) => {
        this.setState({
            gifts: e.target.value
        });
    }
    onChangePromotion = (e) => {
        this.setState({
            promotion: e.target.value
        });
    }
    onChangePromotions = (e) => {
        this.setState({
            promotions: e.target.value
        });
    }
    onChangePk = (e) => {
        this.setState({
            pk: e.target.value
        });
    }
    onChangeGuarantee = (e) => {
        this.setState({
            guarantee: e.target.value
        });
    }
    onChangeGuarantees = (e) => {
        this.setState({
            guarantees: e.target.value
        });
    }
    //thong so san pham
    onChangeManufacturer = (e) => {
        this.setState({
            manufacturer: e.target.value
        });
    }
    onChangeColor = (e) => {
        this.setState({
            color: e.target.value
        });
    }
    onChangeScreen = (e) => {
        this.setState({
            screen: e.target.value
        });
    }
    onChangeRam = (e) => {
        this.setState({
            ram: e.target.value
        });
    }
    onChangeRom = (e) => {
        this.setState({
            rom: e.target.value
        });
    }
    onChangeCpu = (e) => {
        this.setState({
            cpu: e.target.value
        });
    }
    onChangePin = (e) => {
        this.setState({
            pin: e.target.value
        });
    }
    onChangeWaterp = (e) => {
        this.setState({
            waterproof: e.target.value
        });
    }
    onChangeMaterial = (e) => {
        this.setState({
            material: e.target.value
        });
    }

    componentDidMount() {
        axios.get('http://localhost:4000/persons/edit/' + this.props.match.params.id)
            .then(response => {
                this.setState({
                    check: response.data.check,
                    status: response.data.status,
                    date: response.data.date,
                    amounts: response.data.amounts,
                    name: response.data.name,
                    price: response.data.price,
                    sale: response.data.sale,
                    image: response.data.image,
                    image_src: response.data.image_src,
                    detail: response.data.detail,
                    gift: response.data.gift,
                    gifts: response.data.gifts,
                    promotion: response.data.promotion,
                    promotions: response.data.promotions,
                    pk: response.data.pk,
                    guarantee: response.data.guarantee,
                    guarantees: response.data.guarantees,

                    manufacturer: response.data.manufacturer,
                    color: response.data.color,
                    screen: response.data.screen,
                    ram: response.data.ram,
                    rom: response.data.rom,
                    cpu: response.data.cpu,
                    pin: response.data.pin,
                    waterproof: response.data.waterproof,
                    material: response.data.material
                });
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onSubmit = (e) => {
        e.preventDefault();
        const obj = {
            check: this.state.check,
            status: this.state.status,
            date: this.state.date,
            amounts: this.state.amounts,
            name: this.state.name,
            price: this.state.price,
            sale: this.state.sale,
            image: this.state.image,
            image_src: this.state.image_src,
            detail: this.state.detail,
            gift: this.state.gift,
            gifts: this.state.gifts,
            promotion: this.state.promotion,
            promotions: this.state.promotions,
            pk: this.state.pk,
            guarantee: this.state.guarantee,
            guarantees: this.state.guarantees,

            manufacturer: this.state.manufacturer,
            color: this.state.color,
            screen: this.state.screen,
            ram: this.state.ram,
            rom: this.state.rom,
            cpu: this.state.cpu,
            pin: this.state.pin,
            waterproof: this.state.waterproof,
            material: this.state.material
        };
        axios.post('http://localhost:4000/persons/update/' + this.props.match.params.id, obj)
            .then(res => console.log(res.data))
            .then(() => {
                swal("Bạn đã cập nhật thành công.", "", "success");
                setTimeout(() => {
                    window.location.reload();
                }, 1000)
                this.props.history.push('/index');
            })
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/" />
        }
        return (
            <div className="container jumbotron mt-5" style={{ marginTop: 10 }}>
                <h1 className="text-center"><strong>SỬA VÀ CẬP NHẬT SẢN PHẨM</strong></h1><br />
                <form onSubmit={this.onSubmit}>
                    <div className="row text-left">
                        <div className="col-4">
                            <div className="form-group">
                                <label>Check: </label>
                                <input type="text" className="form-control"
                                    value={this.state.check}
                                    onChange={this.onChangeCheck} />
                            </div>
                            <div className="form-group">
                                <label>Status: </label>
                                <input type="text" className="form-control"
                                    value={this.state.status}
                                    onChange={this.onChangeStatus} />
                            </div>
                            <div className="form-group">
                                <label>Date: </label>
                                <input type="datetime-local" className="form-control"
                                    value={this.state.date}
                                    onChange={this.onChangeDate} />
                            </div>
                            <div className="form-group">
                                <label>Amount: </label>
                                <input type="number" className="form-control"
                                    value={this.state.amounts}
                                    onChange={this.onChangeAmount} />
                            </div>
                            <div className="form-group">
                                <label>Name: </label>
                                <input type="text" className="form-control"
                                    value={this.state.name}
                                    onChange={this.onChangeName} />
                            </div>
                            <div className="form-group">
                                <label>Price: </label>
                                <input type="text" className="form-control"
                                    value={this.state.price}
                                    onChange={this.onChangePrice} />
                            </div>
                            <div className="form-group">
                                <label>Sale: </label>
                                <input type="number" className="form-control"
                                    value={this.state.sale}
                                    onChange={this.onChangeSale} />
                            </div>
                            <div className="form-group">
                                <label>Images: </label>
                                <input type="text" className="form-control"
                                    value={this.state.image}
                                    onChange={this.onChangeImage} />
                            </div>
                            <div className="form-group">
                                <label>Image_src: </label>
                                <input type="text" className="form-control"
                                    value={this.state.image_src}
                                    onChange={this.onChangeImage_src} />
                            </div>
                        </div>

                        <div className="col-4">
                            <div className="form-group">
                                <label>Detail: </label>
                                <input type="text" className="form-control"
                                    value={this.state.detail}
                                    onChange={this.onChangeDetail} />
                            </div>
                            <div className="form-group">
                                <label>Gift: </label>
                                <input type="text" className="form-control"
                                    value={this.state.gift}
                                    onChange={this.onChangeGift} />
                            </div>
                            <div className="form-group">
                                <label>Gifts: </label>
                                <input type="text" className="form-control"
                                    value={this.state.gifts}
                                    onChange={this.onChangeGifts} />
                            </div>
                            <div className="form-group">
                                <label>Promotion: </label>
                                <input type="text" className="form-control"
                                    value={this.state.promotion}
                                    onChange={this.onChangePromotion} />
                            </div>
                            <div className="form-group">
                                <label>Promotions: </label>
                                <input type="text" className="form-control"
                                    value={this.state.promotions}
                                    onChange={this.onChangePromotions} />
                            </div>
                            <div className="form-group">
                                <label>Pk: </label>
                                <input type="text" className="form-control"
                                    value={this.state.pk}
                                    onChange={this.onChangePk} />
                            </div>
                            <div className="form-group">
                                <label>Guarantee: </label>
                                <input type="text" className="form-control"
                                    value={this.state.guarantee}
                                    onChange={this.onChangeGuarantee} />
                            </div>
                            <div className="form-group">
                                <label>Guarantees: </label>
                                <input type="text" className="form-control"
                                    value={this.state.guarantees}
                                    onChange={this.onChangeGuarantees} />
                            </div>
                        </div>

                        <div className="col-4">
                            <div className="form-group">
                                <label>Manufacturer: </label>
                                <input type="text" className="form-control"
                                    value={this.state.manufacturer}
                                    onChange={this.onChangeManufacturer} />
                            </div>
                            <div className="form-group">
                                <label>Color: </label>
                                <input type="text" className="form-control"
                                    value={this.state.color}
                                    onChange={this.onChangeColor} />
                            </div>
                            <div className="form-group">
                                <label>screen: </label>
                                <input type="text" className="form-control"
                                    value={this.state.screen}
                                    onChange={this.onChangeScreen} />
                            </div>
                            <div className="form-group">
                                <label>ram: </label>
                                <input type="text" className="form-control"
                                    value={this.state.ram}
                                    onChange={this.onChangeRam} />
                            </div>
                            <div className="form-group">
                                <label>Rom: </label>
                                <input type="text" className="form-control"
                                    value={this.state.rom}
                                    onChange={this.onChangeRom} />
                            </div>
                            <div className="form-group">
                                <label>Cpu: </label>
                                <input type="text" className="form-control"
                                    value={this.state.cpu}
                                    onChange={this.onChangeCpu} />
                            </div>
                            <div className="form-group">
                                <label>Pin: </label>
                                <input type="text" className="form-control"
                                    value={this.state.pin}
                                    onChange={this.onChangePin} />
                            </div>
                            <div className="form-group">
                                <label>Waterproof: </label>
                                <input type="text" className="form-control"
                                    value={this.state.waterproof}
                                    onChange={this.onChangeWaterp} />
                            </div>
                            <div className="form-group">
                                <label>Material: </label>
                                <input type="text" className="form-control"
                                    value={this.state.material}
                                    onChange={this.onChangeMaterial} />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <div className="form-group text-center">
                                <input type="submit" value="Sửa & cập nhật" className="btn btn-lg btn-success btn-block" />
                            </div>
                        </div>
                        <div className="col-6">
                            <Link to="/index" className="btn btn-lg btn-primary btn-block">Xem tất cả sản phẩm</Link>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
