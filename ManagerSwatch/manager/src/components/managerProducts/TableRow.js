import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import swal from '@sweetalert/with-react';

export default class TableRow extends Component {
    delete = () => {
        axios.get('http://localhost:4000/persons/delete/' + this.props.obj._id)
            .then(swal("Bạn đã xóa thành công.", "", "success"))
            .then(setTimeout(() => {
                window.location.reload()
            }, 2000))
            .catch(err => console.log(err))
    }

    render() {
        const { name, price, sale, image, amounts } = this.props.obj;
        return (
            <tr>
                <td>
                    <img src={image} style={{ width: 50, height: 50 }}
                        className="group list-group-image img-thumbnail" alt={name} />
                </td>
                <td>
                    {name}
                </td>
                <td className="text-body">
                    <b><del>{price}</del><sup>đ.</sup></b>
                </td>
                <td className="text-danger">
                    <b>{sale} <sup>đ.</sup></b>
                </td>
                <td>
                    {amounts > 0 ? <b>{amounts}</b> : <b className="text-danger">Hết hàng</b>}
                </td>
                <td>
                    <Link to={"/edit/" + this.props.obj._id} className="btn btn-sm btn-primary">
                        Sửa & Cập nhật
                    </Link>&nbsp;
                    <button onClick={this.delete} className="btn btn-sm btn-danger">
                        Xóa
                    </button>
                </td>
            </tr>
        );
    }
}
