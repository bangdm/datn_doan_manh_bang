const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Business
let Supports = new Schema({
    advisory: {
        type: Number
    },
    date: {
        type: Date
    }
}, {
    collection: 'supports'
});

module.exports = mongoose.model('Supports', Supports);
