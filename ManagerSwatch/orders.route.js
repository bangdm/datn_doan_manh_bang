const express = require('express');
const orderRoutes = express.Router();

// Require Business model in our routes module
let Person = require('./orders.model');

// Defined store route
orderRoutes.route('/add').post(function (req, res) {
    let person = new Person(req.body);
    person.save()
        .then(person => {
            res.status(200).json({ 'order': 'order in added successfully' });
        })
        .catch(err => {
            res.status(400).send("unable to save to database");
        });
});

// Defined get data(index or listing) route
orderRoutes.route('/').get(function (req, res) {
    Person.find(function (err, persons) {
        if (err) {
            console.log(err);
        }
        else {
            res.json(persons);
        }
    });
});

//defined get data bill
orderRoutes.route('/print/:id').get(function (req, res) {
    let id = req.params.id;
    Person.findById(id, function (err, business) {
        res.json(business);
    });
});

// Defined edit route
orderRoutes.route('/edit/:id').get(function (req, res) {
    let id = req.params.id;
    Person.findById(id, function (err, business) {
        res.json(business);
    });
});

//  Defined update route
orderRoutes.route('/update/:id').post(function (req, res) {
    Person.findById(req.params.id, function (err, person) {
        if (!person)
            res.status(404).send("data is not found");
        else {
            console.log(person);
            person.orderCheck = req.body.orderCheck;
            person.nickname = req.body.nickname;
            person.phone = req.body.phone;
            person.email = req.body.email;
            person.address = req.body.address;
            person.note = req.body.note,
                person.date = req.body.date

            person.name = req.body.name;
            person.amount = req.body.amount;
            person.image = req.body.image;
            person.price = req.body.price;
            person.sale = req.body.sale;
            person.color = req.body.color;
            person.advisory = req.body.advisory;
            person.totalPrice = req.body.totalPrice;
            person._id = req.body._id;

            person.save().then(business => {
                res.json('Update complete');
            })
                .catch(err => {
                    res.status(400).send("unable to update the database");
                });
        }
    });
});

// Defined delete | remove | destroy route
orderRoutes.route('/delete/:id').get(function (req, res) {
    Person.findByIdAndRemove({ _id: req.params.id }, function (err, person) {
        if (err) res.json(err);
        else res.json('Successfully removed');
    });
});

module.exports = orderRoutes;
