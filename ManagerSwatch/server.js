const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const server = require('http').Server(app);
const port = process.env.PORT || 4000
const client = require('socket.io')(server);

const Users = require('./routes/Users');
const personRoute = require('./persons.route');
const orderRoute = require('./orders.route');
const supportRoute = require('./routes/support');

const config = require('./DB')

mongoose.Promise = global.Promise
mongoose
  .connect(
    config.DB,
    { useNewUrlParser: true,
      useUnifiedTopology: true,
      keepAlive: true,
      useCreateIndex: true,
      useFindAndModify: false },

    function(err, db) {
      if(err){
        throw err;
    }

    // Connect to Socket.io
    client.on('connection', function(socket){
        let chat = db.collection('chats');

        // Create function to send status
        sendStatus = function(s){
            socket.emit('status', s);
        }

        // Get chats from mongo collection
        chat.find().limit(100).sort({_id:1}).toArray(function(err, res){
            if(err){
                throw err;
            }

            // Emit the messages
            socket.emit('output', res);
        });

        // Handle input events
        socket.on('input', function(data){
            let name = data.name;
            let message = data.message;
            let time = data.time;

            // Check for name and message
            if(name == '' || message == ''){
                // Send error status
                sendStatus('Please enter a name and message');
            } else {
                // Insert message
                chat.insert({name: name, message: message, time: time}, function(){
                    client.emit('output', [data]);

                    // Send status object
                    sendStatus({
                        message: 'Message sent',
                        clear: true
                    });
                });
            }
        });

        // Handle clear
        socket.on('clear', function(data){
            // Remove all chats from collection
            chat.remove({}, function(){
                // Emit cleared
                socket.emit('cleared');
            });
        });
    });
    }
  )
  .then(() => console.log('connected to database'))
  .catch(err => console.log(err))

app.use(bodyParser.json())
app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: false
  })
)

app.use('/users', Users);
app.use('/persons', personRoute);
app.use('/orders', orderRoute);
app.use('/supports', supportRoute);

server.listen(port,() => {
  console.log('Server is running on port: ' + port);
});
